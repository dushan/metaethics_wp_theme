<?php

// TINYMCE-BUTTON
 add_action('init', 'dwgnr_pullquote_shortcode_button_init');
 function dwgnr_pullquote_shortcode_button_init() {
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
         return;
    add_filter("mce_external_plugins", "dwgnr_pullquote_register_tinymce_plugin"); 
    add_filter('mce_buttons', 'dwgnr_pullquote_add_tinymce_button');
}

function dwgnr_pullquote_register_tinymce_plugin($plugin_array) {
  $plugin_array['dwgnr_pullquote_button'] = plugin_dir_url(__FILE__) . 'js/tinymce-plugin.min.js';
  return $plugin_array;
}

function dwgnr_pullquote_add_tinymce_button($buttons) {
  $buttons[] = "dwgnr_pullquote_button";
  return $buttons;
}

// EDITOR-BUTTON
add_action('admin_print_footer_scripts','dwgnr_pullquotes_quicktag');
  function dwgnr_pullquotes_quicktag() {
?>
<script type="text/javascript" charset="utf-8">
  QTags.addButton( 'dwgnr_pullquote', 'pullquote','[pq]', '[/pq]');
</script>
<?php
}