<?php

// STYLESHEET
function dwgnr_pullquotes_style(){
  echo '<style>';
  include_once dirname(__FILE__).'/css/pullquotes.css'; 
  echo '</style>';
}
add_action( 'wp_head', 'dwgnr_pullquotes_style' );