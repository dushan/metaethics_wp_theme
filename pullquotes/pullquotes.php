<?php
/**
 * @package Pullquotes for Write
 * @version 1.0
 */
/*
Plugin Name: Pullquotes for Write
Plugin URI: https://wpwrite.com/sociallinks
Description: Adds pullquotes to you posts and pages. Optimized for <a href="https://wpwrite.com">Write</a> yet usable enywhere.
Author: Dushan Wegner
Version: 1.0
Author URI: https://dushanwegner.com
*/

@include_once 'buttons.php';
@include_once 'shortcode.php';
@include_once 'stylesheet.php';
