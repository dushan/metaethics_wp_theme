<?php

// SHORTCODE
$dwgnr_pullquote_last_align = null; 
function pullquote_render( $atts, $content = null ){
  global $dwgnr_pullquote_last_align;
  extract(shortcode_atts(array(
       'align' => null
    ), $atts));

  // set initial align
  if ($dwgnr_pullquote_last_align == null)
    $dwgnr_pullquote_last_align = has_post_thumbnail() ? 'right' : 'left';

  // set align according to settings and previous align
  if ($align != 'left' && $align != 'right'){
    // user has not set (valid) align via shortcode
    // take opposite of last align setting start with right
    $align = $dwgnr_pullquote_last_align == 'right' ? 'left' : 'right';
  }
  $dwgnr_pullquote_last_align = $align;

  return  '<div class="rendered_pullquote pullquote_'.$align.'">'.
            '<div class="inner">'.$initial_align.$content.'</div>'.
          '</div>';
}
add_shortcode( 'pullquote', 'pullquote_render' );
add_shortcode( 'pq', 'pullquote_render' );