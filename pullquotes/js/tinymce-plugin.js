jQuery(document).ready(function($) {
  tinymce.create('tinymce.plugins.dwgnr_pullquote_plugin', {
    init : function(editor, url) {
      editor.addCommand('dwgnr_pullquote_insert_shortcode', function() {
        selected = tinyMCE.activeEditor.selection.getContent();
        if(selected){
            content =  "[pq]"+selected+'[/pq]';
        }else if (window.dwgnr_pullquotes_opened){
            console.log("opened");
            content =  '[/pq]';
            window.dwgnr_pullquotes_opened = false;
        } else {
            console.log("not opened");
            content =  '[pq]';
            window.dwgnr_pullquotes_opened = true;
        }
        tinymce.execCommand('mceInsertContent', false, content);
      });
      editor.addButton('dwgnr_pullquote_button', {title : 'pullquote', cmd : 'dwgnr_pullquote_insert_shortcode', image: url + '/../images/button.png' });
    },   
  });
  tinymce.PluginManager.add('dwgnr_pullquote_button', tinymce.plugins.dwgnr_pullquote_plugin);
});