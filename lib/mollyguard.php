<?php

/* = Add a "molly guard" to the publish button */
// SRC: https://gist.github.com/plasticmind/4337952
 
add_action( 'admin_print_footer_scripts', 'sr_publish_molly_guard' );
function sr_publish_molly_guard() {
echo <<<EOT
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
  <script>
  jQuery(document).ready(function($){
    $('#publishing-action input[name="publish"]').click(function() {
      if(confirm('Are you sure you want to publish this?')) {
        return true;
      } else {
        $('#publishing-action .spinner').hide();
        $('#publishing-action img').hide();
        $(this).removeClass('button-primary-disabled');
        return false;
      }
    });
  });
  </script>
EOT;
}