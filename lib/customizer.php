<?php

function dwgnr_write_customize_register($wp_customize){;
  if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;
  class Textarea_Custom_Control extends WP_Customize_Control
  {
    public function render_content() {
      ?>
      <label>
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <p class="description customize-section-description"><?php $this->description; ?></p>
        <textarea class="large-text" cols="20" rows="5" <?php $this->link(); ?>>
          <?php echo esc_textarea( $this->value() ); ?>
        </textarea>
      </label>
      <?php
    }
  }

  // REMOVE STATIC-FRONT-PAGE-SETTING
  add_action('customize_register', 'themename_customize_register');
  function themename_customize_register($wp_customize) {
    $wp_customize->remove_section( 'static_front_page' );
  }

  // HOMEPAGE-SECTION
  $wp_customize->add_section('dwgnr_write_homepage', array(
      'title'    => __('Homepage', 'dwgnr'),
      'description' => '',
      'priority' => 100,
  ));
  // Add settings for output description
  $wp_customize->add_setting('dwgnr_write_homepage_sort_order', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  // Add control and output for select field
  $wp_customize->add_control('dwgnr_write_homepage_sort_order', array(
      'label'      => __( 'Random post order', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '0'
  ) );
  // SHOW IMAGES ON HOMEPAGE
  $wp_customize->add_setting('dwgnr_write_homepage_shows_images', array(
      'capability' => 'edit_theme_options',
      'default'     => 1
  ) );
  $wp_customize->add_control('dwgnr_write_homepage_shows_images', array(
      'label'      => __( 'Homepage shows images', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );
  // BODY ONLY ON HOMEPAGE, LINK TO FIRST LINK
  $wp_customize->add_setting('dwgnr_write_homepage_news_link_mode', array(
      'capability' => 'edit_theme_options',
      'default'     => 0
  ) );
  $wp_customize->add_control('dwgnr_write_homepage_news_link_mode', array(
      'label'      => __( 'show only post-body and link it to first url in body', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );
  // SHOW DOMAIN OF LINKS WHEN BODY-ONLY
  $wp_customize->add_setting('dwgnr_write_homepage_show_domain', array(
      'capability' => 'edit_theme_options',
      'default'     => 0
  ) );
  $wp_customize->add_control('dwgnr_write_homepage_show_domain', array(
      'label'      => __( 'show domain on body-only-posts', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );
  // SHOW DATE LINKS WHEN BODY-ONLY
  $wp_customize->add_setting('dwgnr_write_homepage_show_date', array(
      'capability' => 'edit_theme_options',
      'default'     => 0
  ) );
  $wp_customize->add_control('dwgnr_write_homepage_show_date', array(
      'label'      => __( 'show date on body-only-posts', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );
  // COLUMNS OR LINES ON HOMEPAGE
  $wp_customize->add_setting('dwgnr_write_homepage_show_boxes', array(
      'capability' => 'edit_theme_options',
      'default'     => 0
  ) );
  $wp_customize->add_control('dwgnr_write_homepage_show_boxes', array(
      'label'      => __( 'show boxes instead of lines (always, when images)', 'dwgnr' ),
      'section'    => 'dwgnr_write_homepage',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );


  // DESIGN-SECTION
  $wp_customize->add_section('dwgnr_write_design', array(
      'title'    => __('Design & Stuff', 'dwgnr'),
      'description' => '',
      'priority' => 120,
  ));

  // PRIMARY COLOR
  $wp_customize->add_setting('dwgnr_write_color_1', array(
      'default'        => '#ff0000',
      'capability'     => 'edit_theme_options',
      'type'           => 'option',
  ));
  $wp_customize->add_control( 
    new WP_Customize_Color_Control( 
    $wp_customize, 
    'dwgnr_write_color_1', 
    array(
      'label'      => __( 'Primary Color', 'dwgnr' ),
      'section'    => 'dwgnr_write_design',
      'settings'   => 'dwgnr_write_color_1',
    ) ) 
  );
  // COLOR OF TYPO ON PRIMARY COLOR
  $wp_customize->add_setting('dwgnr_write_color_of_typo_on_color_1', array(
      'default'        => '#ffffff',
      'capability'     => 'edit_theme_options',
      'type'           => 'option',
  ));
  $wp_customize->add_control( 
    new WP_Customize_Color_Control( 
    $wp_customize, 
    'dwgnr_write_color_of_typo_on_color_1', 
    array(
      'label'      => __( 'Color of Typo on Primary Color', 'dwgnr' ),
      'section'    => 'dwgnr_write_design',
      'settings'   => 'dwgnr_write_color_of_typo_on_color_1',
    ) ) 
  );
  // SHOW AUTHOR-BOX
  $wp_customize->add_setting('dwgnr_write_show_authorbox_under_post', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  $wp_customize->add_control('dwgnr_write_show_authorbox_under_post', array(
      'label'      => __( 'show authorbox under post', 'dwgnr' ),
      'section'    => 'dwgnr_write_design',
      'type'       => 'checkbox',
      'std'         => '1'
  ) );


  // SCRIPT BEFORE FINAL BODY TAG
  $wp_customize->add_section('dwgnr_finalscript', array(
      'title'    => __('Page-Footer', 'dwgnr'),
      'description' => __('Inserted before the ending &lt;/body&gt;-tag. Add &lt;script&gt;-tags if needed.','dwgnr'),
      'priority' => 200,
  ));
  $wp_customize->add_setting('dwgnr_finalscript_content', array(
      'default'        => '',
      'capability'     => 'edit_theme_options',
      'type'           => 'option',
  ));
  $wp_customize->add_control( new Textarea_Custom_Control( $wp_customize, 'dwgnr_finalscript_content', array(
      'section'     => 'dwgnr_finalscript',
      'settings'    => 'dwgnr_finalscript_content',
      'priority'    => 10,
  )));
  $wp_customize->add_setting('dwgnr_finalscript_hide_when_writing_rights', array(
      'capability' => 'edit_theme_options',
      'default'   =>  'show_always',
      'type'       => 'option'
  ));  
  $wp_customize->add_control('display_header_text', array(
      'settings'  => 'dwgnr_finalscript_hide_when_writing_rights',
      'label'     => __('display options for visitor-status', 'dwgnr'),
      'section'   => 'dwgnr_finalscript',
      'type'      => 'select',
      'choices'   => array(
          'show_always'                       => __('show always', 'dwgnr'),
          'deactivate'                        => __('deactivate', 'dwgnr'),
          'hide_for_users_with_writing_rights'       => __('hide for users with writing rights', 'dwgnr'),
          'show_for_users_with_writing_rights_only'  => __('show only for users with writing rights', 'dwgnr'),
      ),
  ));

  $wp_customize->add_setting('dwgnr_footer_copyright_line', array(
      'default'        => '',
      'capability'     => 'edit_theme_options',
      'type'           => 'option',
  ));
  $wp_customize->add_control( new Textarea_Custom_Control( $wp_customize, 'dwgnr_footer_copyright_line', array(
      'label'       => __('Line before Template-Credits', 'dwgnr'),
      'section'     => 'dwgnr_finalscript',
      'settings'    => 'dwgnr_footer_copyright_line',
      'priority'    => 10,
  )));

  // NOSCRIPT-SECTION
  $wp_customize->add_section('dwgnr_write_noscript', array(
      'title'    => __('NOSCRIPT', 'dwgnr'),
      'description' => '',
      'priority' => 100,
  ));
  // Add settings for output description
  $wp_customize->add_setting('dwgnr_write_noscript_show_warning', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  // Add control and output for select field
  $wp_customize->add_control('dwgnr_write_noscript_show_warning', array(
      'label'      => __( 'Show noscript-warning', 'dwgnr' ),
      'section'    => 'dwgnr_write_noscript',
      'type'       => 'checkbox',
      'std'         => '0'
  ) );
  // content of noscript-field
  $wp_customize->add_setting('dwgnr_write_noscript_context', array(
      'default'        => '',
      'capability'     => 'edit_theme_options',
      'type'           => 'option',
  ));
  $wp_customize->add_control( new Textarea_Custom_Control( $wp_customize, 'dwgnr_write_noscript_context', array(
      'section'     => 'dwgnr_write_noscript',
      'settings'    => 'dwgnr_write_noscript_context',
      'priority'    => 10,
  )));
  // –––––––
  // 

}
add_action('customize_register', 'dwgnr_write_customize_register');