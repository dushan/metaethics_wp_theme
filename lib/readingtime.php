<?php
function dwgnr_english_readingtime() {
  $post = get_post();
  $words = str_word_count( strip_tags( $post->post_content ) );
  $minutes = floor( $words / 200 );
  $seconds = floor( $words % 200 / ( 200 / 60 ) );
  $seconds = 10 * floor($seconds / 10);

  if ( 1 <= $minutes ) {
    $timestring = $minutes . ' minute' . ($minutes == 1 ? '' : 's');
    if ($seconds != 0) {
      $timestring .= ', ' . $seconds . ' second' . ($seconds == 1 ? '' : 's');
    }
  } else {
    $timestring = __('less than a minute');
  }
  return $timestring;
}