<?php

$end_script_status = get_option('dwgnr_finalscript_hide_when_writing_rights', 'show_always');
if  ( $end_script_status == 'show_always' ||
      (current_user_can('edit_posts') && $end_script_status == 'show_for_users_with_writing_rights_only') ||
      (!current_user_can('edit_posts') && $end_script_status == 'hide_for_users_with_writing_rights')
    ) {
  echo get_option('dwgnr_finalscript_content');
}
