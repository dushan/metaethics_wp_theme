<div id="authorbox" class="column small-12 row">
  <div class="authoricon">
    <?php echo get_avatar( get_the_author_meta( 'user_email' ), 120 ); ?>
  </div>
  <div class="authorcontent">
    <h2><?php the_author(); ?></h2>
    <p>
      <?php the_author_meta( 'description' ); ?>
      <!-- <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">View all posts by <?php get_the_author(); ?> <span class="meta-nav">&rarr;</span></a> -->
    </p>
  </div>
</div>