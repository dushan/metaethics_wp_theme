<?php

function posts_on_homepage( $query ) {
  if ( $query->is_home() && $query->is_main_query() ) {
    $query->set( 'posts_per_page', 9 );
    // $query->set( 'ignore_sticky_posts', false );
  }
}
add_action( 'pre_get_posts', 'posts_on_homepage' );