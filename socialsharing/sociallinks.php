<?php
/**
 * @package Social Links for Write
 * @version 1.0
 */
/*
Plugin Name: Social Links for Write
Plugin URI: https://dwgnr_write.com/sociallinks
Description: Adds HTML-links for social sharing, optimized for the theme <a href="https://dwgnr_write.com">Write</a>, yet usable enywhere.
Author: Dushan Wegner
Version: 1.0
Author URI: https://dushanwegner.com
*/

@include_once 'customizer.php';
@include_once 'stylesheet.php';
@include_once 'rendercontent.php';
