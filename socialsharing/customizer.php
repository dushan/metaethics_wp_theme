<?php

function write_sociallinks_customizer($wp_customize) {


  // SOCIAL SHARING
  $wp_customize->add_section('dwgnr_write_social_handles', array(
      'title'    => __('Social', 'dwgnr'),
      'description' => '',
      'priority' => 150,
  ));
  // Github handle
  $wp_customize->add_setting('dwgnr_write_github_handle', array(
      'default'         => '',
      'description'     => __('', 'dwgnr'),
      'capability'      => 'edit_theme_options',
      'type'            => 'option',
  ));
  $wp_customize->add_control('dwgnr_write_github_handle', array(
      'label'      => __('Github-Handle', 'dwgnr'),
      'section'    => 'dwgnr_write_social_handles',
      'settings'   => 'dwgnr_write_github_handle',
  ));

  // Twitter handle
  $wp_customize->add_setting('dwgnr_write_twitter_handle', array(
      'default'         => '',
      'capability'      => 'edit_theme_options',
      'type'            => 'option',
  ));
  $wp_customize->add_control('dwgnr_write_twitter_handle', array(
      'label'      => __('Twitter-Handle', 'dwgnr_write_sociallinks'),
      'description'     => __('If your Twitter-handle were <a href="https://twitter.com/dushanwegner" target="_blank">@dushanwegner</a> you would enter <code style="font-family:monospace;font-style:normal;">dushanwegner</code>.', 'dwgnr_write_sociallinks'),
      'section'    => 'dwgnr_write_social_handles',
      'settings'   => 'dwgnr_write_twitter_handle',
  ));

  // Facebook handle
  $wp_customize->add_setting('dwgnr_write_facebook_handle', array(
      'default'         => '',
      'description'     => __('', 'dwgnr'),
      'capability'      => 'edit_theme_options',
      'type'            => 'option',
  ));
  $wp_customize->add_control('dwgnr_write_facebook_handle', array(
      'label'      => __('Facebook-Path', 'dwgnr'),
      'section'    => 'dwgnr_write_social_handles',
      'settings'   => 'dwgnr_write_facebook_handle',
  ));
  // facebook bar
  $wp_customize->add_setting('dwgnr_write_show_facebook_like_in_headbar', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  $wp_customize->add_control('dwgnr_write_show_facebook_like_in_headbar', array(
      'label'      => __( 'Show Facebook-bar in header', 'dwgnr' ),
      'section'    => 'dwgnr_write_social_handles',
      'type'       => 'checkbox',
      'std'         => '0'
  ) );
  $wp_customize->add_setting('dwgnr_text_before_facebook_like_in_headbar', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  $wp_customize->add_control('dwgnr_text_before_facebook_like_in_headbar', array(
      'label'      => __( 'Text before Facebook-like-button', 'dwgnr' ),
      'section'    => 'dwgnr_write_social_handles',
      'default'    => __('Follow us on Facebook →', 'dwgnr')
  ) );
  $wp_customize->add_setting('dwgnr_short_text_before_facebook_like_in_headbar', array(
      'type'       => 'option',
      'capability' => 'edit_theme_options'
  ) );
  $wp_customize->add_control('dwgnr_short_text_before_facebook_like_in_headbar', array(
      'label'      => __( 'Short text before Facebook-like-button', 'dwgnr' ),
      'section'    => 'dwgnr_write_social_handles',
      'default'    => __('Follow us →', 'dwgnr')
  ) );

  // Instagram handle
  $wp_customize->add_setting('dwgnr_write_instagram_handle', array(
      'default'         => '',
      'description'     => __('', 'dwgnr'),
      'capability'      => 'edit_theme_options',
      'type'            => 'option',
  ));
  $wp_customize->add_control('dwgnr_write_instagram_handle', array(
      'label'      => __('Instagram-Handle', 'dwgnr'),
      'section'    => 'dwgnr_write_social_handles',
      'settings'   => 'dwgnr_write_instagram_handle',
  ));
  // Pinterest handle
  $wp_customize->add_setting('dwgnr_write_pinterest_handle', array(
      'default'         => '',
      'description'     => __('', 'dwgnr'),
      'capability'      => 'edit_theme_options',
      'type'            => 'option',
  ));
  $wp_customize->add_control('dwgnr_write_pinterest_handle', array(
      'label'      => __('Pinterest-Handle', 'dwgnr'),
      'section'    => 'dwgnr_write_social_handles',
      'settings'   => 'dwgnr_write_pinterest_handle',
  ));


}
add_action('customize_register', 'write_sociallinks_customizer');
