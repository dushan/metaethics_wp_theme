<?php

// STYLESHEET
function dwgnr_write_sociallinks_style(){
  echo '<style>';
  include_once dirname(__FILE__).'/css/sociallinks.css'; 
  echo '</style>';
}
add_action( 'wp_head', 'dwgnr_write_sociallinks_style' );
