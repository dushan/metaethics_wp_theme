<?php

function dwgnr_write_sociallinks_cleanup_string($content){
  $content = urlencode($content);
  $content = str_replace('%22', "'", $content);
  $content = str_replace('%21', "!", $content);
  $content = str_replace('%26%238217%3B', "'", $content);
  $content = str_replace('%26%238220%3B', "›", $content);
  $content = str_replace('%26%238221%3B', "‹", $content);
  return $content;
}

function dwgnr_write_isIphone($user_agent=NULL) {
    if(!isset($user_agent)) {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    return (strpos($user_agent, 'iPhone') !== FALSE);
}


function dwgnr_write_sociallinks_render($content){
  if(is_single()): 
    $ret = '<p class="dwgnr_write_sociallinks column small-12 row collapse">';
    if (function_exists('wpbitly_shortlink')) {
      $xml = simplexml_load_string(wpbitly_shortlink());
      $list = $xml->xpath("//@href");
      $preparedUrls = array();
      foreach($list as $item) {
          $item = parse_url($item);
          $url_without_protocol = $item['host'] . $item['path'];
          $url = $item['scheme'] . '://' .  $item['host'] . $item['path'];
      }
    } else {
      $url = get_the_permalink();
    }
    $full_title = get_the_title();
    $abbr_title = (strlen($full_title) > 93) ? substr($full_title,0,90).'...' : $full_title;
    $short_abbr_title = (strlen($full_title) > 53) ? substr($full_title,0,50).'...' : $full_title;

    // STRING FOR TWITTER
    $twitterstring = '»'.dwgnr_write_sociallinks_cleanup_string($abbr_title).'« '.$url;
    $twitterstring .= ' '.__('via','dwgnr').' @'.get_option('dwgnr_write_twitter_handle');

    // MAILSTRING
    $mailstring = dwgnr_write_sociallinks_cleanup_string('»'. $full_title . "«\n\n" . $url);
    $mailsubject = urlencode(get_option('dwgnr_social_sharing_mail_subject', __('something you might like', 'dwgnr_write_sociallinks')));

    // APP-PROTOCOLs for iPhone
    if (dwgnr_write_isIphone()) {
      $twitterurl = 'twitter://post?message=';
      $facebookurl = 'fb';
    } else {
      $twitterurl = '//twitter.com/home?status=';
      $facebookprotocol = 'https';
    }

    $ret .= '<a class="dwgnr_social_link column twitter small-4" href="'.$twitterurl . $twitterstring . '" target="_blank"><i class="fa fa-twitter"></i> <span class="hide-for-small-only">' . __('Twitter', 'dwgnr_write_sociallinks') .'</span></a>';
    $ret .= '<a class="dwgnr_social_link column facebook small-4" href="'.$facebookprotocol.'://facebook.com/sharer/sharer.php?u='. $url . '" target="_blank"><i class="fa fa-facebook"></i> <span class="hide-for-small-only">' . __('Facebook', 'dwgnr_write_sociallinks') . '</span></a>';
    $ret .= '<a class="dwgnr_social_link column mail small-4" href="mailto:?subject=' . $mailsubject . '&body=' . $mailstring . '"><i class="fa fa-envelope-o"></i> <span class="hide-for-small-only">' . __('E-Mail', 'dwgnr_write_sociallinks') . '</span></a>';
    $ret .= '</p><br clear="all">';
    $content = $ret . $content . $ret;
  endif;
  return $content;
}
add_action('the_content', 'dwgnr_write_sociallinks_render');