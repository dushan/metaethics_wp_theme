<?php
get_header();
flush();

if (is_tag()) {
  single_tag_title('<div class="row column small-12"><h2>','</h2></div>');
}

?>
<div id="content" class="home row" data-equalizer data-equalize-by-row="false">
  <?php

  $counter = 0;

  if (get_option('dwgnr_write_homepage_sort_order') == '1') {
    $wp_query = new WP_Query( array( orderby => 'rand', 'posts_per_page' => 100 ) );
  }

  if ( $wp_query->have_posts() ) :
    while ($wp_query->have_posts() ) : $wp_query->the_post();
      $counter++;

      // echo ">>> " . get_theme_mod('dwgnr_write_homepage_shows_images');

      if (get_theme_mod('dwgnr_write_homepage_show_boxes') == 1 || get_theme_mod('dwgnr_write_homepage_shows_images') == 1) {
      ?>
      <article class="column small-6 large-4 end">
      <?php
      } else { ?>
      <article class="column small-12">
      <?php } ?>
        <a href="<?php the_permalink(); ?>">
        <?php if(get_theme_mod('dwgnr_write_homepage_shows_images') == 1) { 
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small_article_image'); 
            $small_url = $thumb['0'];
            $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article_image'); 
            $medium_url = $thumb['0'];
          ?>
          <img data-interchange="[<?php echo $small_url; ?>, small], [<?php echo $medium_url; ?>, medium]">
          <noscript>
            <img src="<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article_image'); echo $thumb['0']; ?>">
          </noscript>

          <?php } ?>
          <!--<div class="inner" <?php if (get_theme_mod('dwgnr_write_homepage_shows_images') == 1) { echo 'data-equalizer-watch'; } ?>>-->
          <div class="inner" data-equalizer-watch>
            <?php

            // NEWS-MODE
            if (get_theme_mod('dwgnr_write_homepage_news_link_mode')) {

              $string = get_the_content();
              $url = explode(' ',strstr($string,'http'))[0]; //"prints" http://example.com
              $payload = str_replace($url, '', $string);

              if (get_theme_mod('dwgnr_write_homepage_show_domain') || get_theme_mod('dwgnr_write_homepage_show_date')) {
              echo "<div class=\"domain_and_or_url\">";
                if (get_theme_mod('dwgnr_write_homepage_show_domain')) {
                  echo "<strong>";
                  $urlAsSource = parse_url($url);
                  $domain = $urlAsSource['host'];
                  $domain = str_replace('www.', '', $domain);
                  echo $domain.' ';
                  echo "</strong>";
                }
                echo "<span class=\"hide-for-small-only\">";
                if (get_theme_mod('dwgnr_write_homepage_show_date')) {
                  echo get_post_time('m/d H:i', true).' GMT';
                }
                echo "</span>";
              echo "</div>";
              }

              echo "<h2><a href=\"$url\">$payload</a>";
              if (current_user_can('edit_post')) {
                echo "<small><a href=\"".get_edit_post_link()."\">edit</a></smalL>";
              }
              echo "</h2>";

            } else {
              ?>
              <div class="readingtime hide-for-small-only">
                <?php
                // echo get_the_tag_list('',', ','') . ' – ';
                echo __('read this in ') . ' ' . dwgnr_english_readingtime(); ?>
              </div>
              <h2><?php the_title(); ?></h2>
              <?php
            }
            ?>
          </div>
        </a>
      </article>
    <?php
      // prerendering the first 3 articles as these are most likely to be clicked
      if ($counter <= 3) echo '<link rel="prerender" href="'.get_the_permalink().'">';
      flush();
    endwhile;
    wp_reset_postdata();
  endif; ?>           
  </div>
</div>
<div class="row">
  <?php dynamic_sidebar( 'homepage_info' ); ?>
</div>
<?php
require_once('footer.php');
