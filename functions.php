<?php

add_theme_support( 'automatic-feed-links' );
register_nav_menus(array(
  'footer_menu' => __('Menu in Footer', 'dwgnr')
));
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

include('lib/readingtime.php');
include('lib/customizer.php');

// SOCIAL SHARING
include('socialsharing/write-sociallinks.php');


add_theme_support( 'post-thumbnails' ); 
set_post_thumbnail_size( 445, 445, array( 'center', 'center') );
add_image_size( 'article_image', 300, 200, array( 'center', 'center' ) );

if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'portrait', 200, 300, array( 'center', 'center' ) );
  add_image_size( 'square', 300, 300, array( 'center', 'center' ) );
  add_image_size( 'article_image', 300, 200, array( 'center', 'center' ) );
  add_image_size( 'small_article_image', 150, 100, array( 'center', 'center' ) );
  add_image_size( 'small_square', 100, 100, array( 'center', 'center' ) );
  }
  add_filter('image_size_names_choose', 'my_image_sizes');
  function my_image_sizes($sizes) {
  $addsizes = array(
  "portrait" => __( "Portrait"),
  "square" => __( "Square"),
  "article_image" => __( "Article Image")
  );
  $newsizes = array_merge($sizes, $addsizes);
  return $newsizes;
}


load_theme_textdomain( 'dwgnr', get_template_directory().'/' );

if ( !current_user_can( 'unfiltered_html' ) ) {
  add_filter( 'w3tc_can_print_comment', function( $w3tc_setting ) { return false; }, 10, 1 );
}

// FOUNDATION VIDEO-PLAYER
add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    $return = '<div class="flex-video">'.$html.'</div>';
    return $return;
}

// WIDGETS
function theme_widgets_init() {

  register_sidebar( array(
    'name'          => 'Sidebar Post',
    'id'            => 'sidebar_post',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Sidebar Page',
    'id'            => 'sidebar_page',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Underpost',
    'id'            => 'underpost',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Homepage Info',
    'id'            => 'homepage_info',
    'before_widget' => '<div class="column small-12 medium-6 push-4 large-4">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

}
add_action( 'widgets_init', 'theme_widgets_init' );

include_once 'lib/mollyguard.php'; 

function remove_comments_rss( $for_comments ) {
    return;
}
add_filter('post_comments_feed_link','remove_comments_rss');


add_action('wp_enqueue_scripts', 'remove_css', 20);
function remove_css() {
    if (!is_admin()) {
        wp_dequeue_style('views-pagination-style');
        wp_dequeue_script('views-pagination-script');
        wp_dequeue_script('jquery-ui-datepicker');
        wp_dequeue_script('jquery');
    }
}

// if (is_admin()) {
//   echo '<h1>ADMIN</h1>';
// } else {
//   echo '<h1>NO ADMIN</h1>';
// }

if ( !is_admin() && !isset($_REQUEST['wp_customize']) ) {
  function jquery_loadup() {
          wp_deregister_script('jquery');
          wp_deregister_script('jquery-migrate.min');
  }
  add_action('wp_enqueue_scripts','jquery_loadup');
}

function my_assets() {
  if (!is_admin()) {
    wp_enqueue_script( 'myjquery', get_stylesheet_directory_uri() . '/bower_components/jquery/dist/jquery.js');
  }
  wp_enqueue_script( 'jquery.cookie', get_stylesheet_directory_uri() . '/bower_components/jquery.cookie/jquery.cookie.js' );
  wp_enqueue_script( 'foundation.min', get_stylesheet_directory_uri() . '/bower_components/foundation-sites/dist/foundation.min.js' );
  wp_enqueue_script( 'foundation.equalizer', get_stylesheet_directory_uri() . '/bower_components/foundation-sites/js/min/foundation.equalizer-min.js' );
  wp_enqueue_script( 'fastclick', get_stylesheet_directory_uri() . '/bower_components/fastclick/lib/fastclick.js' );
  wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js' );

  wp_enqueue_script( 'myjquery' );
  wp_enqueue_script( 'jquery.cookie' );
  wp_enqueue_script( 'foundation.min' );
  wp_enqueue_script( 'foundation.equalizer' );
  wp_enqueue_script( 'fastclick' );
  wp_enqueue_script( 'main' );
}
add_action( 'wp_enqueue_scripts', 'my_assets' );

