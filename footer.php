    <?php
    flush();
    wp_footer();
    ?>
    <footer class="row">
      <div class="column small-12">
        <div class="inner">
          <?php 
            wp_nav_menu(array('theme_location' => 'footer_menu', 'container' => false, 'menu_class' => 'footer_menu'));
          ?>
          <div id="poweredby">powered by <a href="http://wordpress.org" target="_blank">Wordpress</a> &amp; <a href="http://metaethics.org/free-wordpress-viral-theme" alt="free viral wordpress theme by Metaethics.org">Metaethics.org</a></div>
        </div>
      </div>
    </footer>
    <?php include 'lib/endscripts.php'; ?>
    <link rel="prerender" href="<?php echo bloginfo('url'); ?>">
  </body>
</html>