<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/bower_components/foundation-sites/dist/foundation.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/bower_components/font-awesome/css/font-awesome.min.css" media="screen" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title><?php wp_title( '&mdash;' ); ?></title>
    <?php
      wp_head();
      if (get_option('dwgnr_write_color_of_typo_on_color_1')) {
        echo '<style>';
        echo ' #header h1, #header #description, #header a { color: '.get_option('dwgnr_write_color_of_typo_on_color_1').'; }';
        echo '</style>';
      }
    ?>
  </head>
  <body <?php body_class(); ?>>


    <?php    if (get_option('dwgnr_write_facebook_handle')) { ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.async=true; js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.5&appId=463809083807366";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <?php } ?>

    <div id="headerwrapper"
      <?php 
        if (get_option('dwgnr_write_color_1')) {
          echo ' style="background-color:'.get_option('dwgnr_write_color_1').';"';
        }
      ?>
      >
    <div id="header" class="row">
      <a href="<?php bloginfo('url'); ?>">
        <div class="small-12 medium-7 column  text-center medium-text-left">
          <h1><?php bloginfo( 'name' ); ?></h1>
          <p id="description"><?php bloginfo( 'description' ); ?></p>
        </div> 
      </a>
      <div id="socialicons" class="hide-for-small-only column small-12 medium-5 text-center medium-text-right">
        <?php 


        // Github
        if (get_option('dwgnr_write_github_handle')) {
          echo '<a href="https://github.com/'.get_option('dwgnr_write_github_handle').'" target="_blank"><i class="fa fa-github" alt="Github-Icon"></i></a>';
        }

        // Pinterest
        if (get_option('dwgnr_write_pinterest_handle')) {
          echo '<a href="https://pinterest.com/'.get_option('dwgnr_write_pinterest_handle').'" target="_blank"><i class="fa fa-pinterest" alt="Pinterest-Icon"></i></a>';
        }

        // Twitter
        if (get_option('dwgnr_write_twitter_handle')) {
          echo '<a href="https://twitter.com/'.get_option('dwgnr_write_twitter_handle').'" target="_blank"><i class="fa fa-twitter" alt="Twitter-Icon"></i></a>';
        }

        // Facebook
        if (get_option('dwgnr_write_facebook_handle')) {
          echo '<a href="https://facebook.com/'.get_option('dwgnr_write_facebook_handle').'" target="_blank"><i class="fa fa-facebook" alt="Facebook-Icon"></i></a>';
        }

        // Instagram
        if (get_option('dwgnr_write_instagram_handle')) {
          echo '<a href="https://instagram.com/'.get_option('dwgnr_write_instagram_handle').'" target="_blank"><i class="fa fa-instagram" alt="Instagram-Icon"></i></a>';
        }

        ?>
      </div>
    </div><!-- header -->
    <?php 
      if(get_option('dwgnr_write_show_facebook_like_in_headbar')):
    ?>
    <div id="dwgnr_facebook_topbar">
      <div class="row">
        <div class="column small-12 inner">
          <span class="hide-for-small-only"><?php echo get_option('dwgnr_text_before_facebook_like_in_headbar'); ?></span>
          <span class="show-for-small-only"><?php echo get_option('dwgnr_short_text_before_facebook_like_in_headbar'); ?></span>
          <div class="fbwrapper"><div class="fb-like" data-href="https://www.facebook.com/<?php echo get_option('dwgnr_write_facebook_handle'); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div></div>



        </div>
      </div>
    </div>
    <?php
      endif;
    ?>
    <?php 
      if(get_option('dwgnr_write_noscript_show_warning')):
    ?>
    <noscript>
    <div id="dwgnr_write_noscript_warning">
      <div class="row">
        <div class="column small-12 inner">
          <span><?php echo get_option('dwgnr_write_noscript_context'); ?></span>
        </div>
      </div>
    </div>
    <?php
      endif;
    ?>
    </noscript>
  </div><!-- headerwrapper -->
  <?php flush();
