<?php include 'lib/head.php'; ?>
<div id="content">
  <ul class="post_list">
    <?php

    $current_category = get_the_category();
    $cat_id = $current_category[0]->cat_ID;

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = array(
        'cat' => $cat_id,
        'orderby' => 'date',
        'paged' => $paged
      ); 
    query_posts($args);
    $count = 0;
    if ( have_posts() ) : while ( have_posts() ) : the_post();
      $count++;
      ?>
      <li><a href="<?php the_permalink(); ?>">
        <?php
          echo '<div class="date">';
          $post_date = mysql2date("Y", $post->post_date_gmt);
          if(date("Y") == $post_date){
            the_time(get_option('dwgnr_frontpage_dateformat_this_year', 'j.n.'));
          } else {
            the_time(get_option('dwgnr_frontpage_dateformat_previous_year', 'j.n.Y'));
          }
          echo '</div>';
        ?>
        <span class="<?php if(is_sticky()) { echo 'sticky';} ?>"><?php the_title(); ?></span>
      </a></li>
      <?php if ($count%3==10) flush(); // send data every 10 posts ?>
    <?php endwhile; ?>
  </ul>
  <?php else: ?>
    <div class="hentry page404"><h2><?php echo __('Right now there is no page for the URL you entered…', 'dwgnr'); ?></h2></div>
  <?php endif; ?>
  <div class="float_wrapper under_post_navigation">
    <div class="float_left previous"><?php next_posts_link( 'frühere Texte' ); ?></div>
    <div class="float_right next"><?php previous_posts_link( 'spätere Texte' ); ?></div>
  </div>
  <div class="clear"> </div>
</div><!-- content -->
<?php require_once('footer.php'); ?>
