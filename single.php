<?php get_header(); ?>
<div id="content" class="row ">
  <h2 class="content-title column small-12 medium-10 large-12">
    <?php the_title(); ?>
    <div class="readingtime"><?php
      echo '<span class="hide-for-small-only">' . __('reading time') . ': </span> ' . dwgnr_english_readingtime();
    ?></div>
  </h2>
  <?php if ( have_posts() ) : the_post(); ?>
    <div <?php post_class('column small-12 medium-10 large-8'); ?>>
      <div id="content_inner_wrapper" clear="all">
        <?php
          the_content();
          flush();
          include_once dirname(__FILE__).'/lib/authorbox.php';
          include_once dirname(__FILE__).'/underpost.php'; ?>
      </div>
    </div>
  <?php else: ?>
    <div class="hentry page404"><h2><?php echo __('Right now there is no page for the URL you entered…', 'dwgnr'); ?></h2></div>
  <?php endif; ?>
  <div id="sidebar" class="column small-12 medium-10 large-4 end">
    <div class="inner column">
      <?php dynamic_sidebar( 'sidebar_post' ); ?>
    </div>
  </div>
</div><!-- content -->
<?php require_once('footer.php'); ?>