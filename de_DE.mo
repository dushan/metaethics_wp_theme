��          �       �      �     �     �     �     �          -  :   B  �   }  	     P   !     r     �     �     �     �  5   �               +  ,   4     a  
   p  "   {  "   �     �  '   �  r  �     h     y     �      �      �     �  3   �  �   '	  
   �	  9   �	     
     %
     =
     Q
     a
  2   o
     �
     �
     �
  *   �
          "  !   /  '   Q     y  )   �   Box 1 Color Box 2 Color Box 3 Color Date-format for current year Date-format for previous years First menu in footer Format for the date when the post is from a previous year. Format for the date when the post is from the current year. <a href="http://php.net/manual/de/function.date.php" target="_blank">Formatting PHP-Dates</a> Frontpage Inserted before the ending &lt;/body&gt;-tag. Add &lt;script&gt;-tags if needed. Line before Template-Credits Link via yourdomain.com Mail Subject Newsletter-Link Page-Footer Right now there is no page for the URL you entered… Second menu in footer Social Sharing TOC Menu Text that links to newsletter-subscribe-form Twitter-Handle deactivate display options for visitor-status hide for users with writing rights show always show only for users with writing rights Project-Id-Version: Dushan Wegner
Report-Msgid-Bugs-To: 
POT-Creation-Date: Tue Jun 23 2015 21:29:21 GMT+0200 (CEST)
PO-Revision-Date: Thu Jul 16 2015 09:32:37 GMT+0200 (CEST)
Last-Translator: dushan <d@wgnr.es>
Language-Team: 
Language: German
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: de_DE
X-Generator: Loco - https://localise.biz/ Farbe für Box 1 Farbe für Box 2 Farbe für Box 3 Datumsformat für aktuelles Jahr Datumsformat für frühere Jahre Erstes Menu im Footer Datumsformat wenn der Post aus früheren Jahren ist Datumsformat wenn der Post aus dem aktuellen Jahr ist. <a href="http://php.net/manual/de/function.date.php" target="_blank">Datumformatierung in PHP</a> Startseite Wir vor dem 
 &lt;/body&gt;-
Tag in die Seite eingesetzt. Zeile vor den Credits im Footer Link via deinedomain.de E-Mail Betreffzeile Newsletter-Link Seiten-Footer Im Moment gibt es keine Seite hier mit dieser URL. Zweites Menu im Footer Teilen in Social Media Menu Inhaltsverzeichnis Text, der zum Newsletter-Abo-Formular link Twitter-Handle deaktivieren Anzeige-Optionen für User-Status für User mir Schreibrechten verstecken immer anzeigen nur für User mir Schreibrechten anzeigen 